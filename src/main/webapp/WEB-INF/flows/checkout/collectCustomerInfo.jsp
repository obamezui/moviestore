<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 10/04/2016
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>

<div class="content">
    <div class="register">
        <form:form   commandName="order">
            <div class="register-top-grid">
                <h1>Facturation</h1>
                <div>
                    <span>Nom<label for="customerName">*</label></span>
                    <form:input path="cart.customer.customerName"  id="customerName" class="form-control"/>
                </div>
                <div>
                    <span>Email<label for="customerEmail">*</label></span>
                    <form:input path="cart.customer.customerEmail"  id="customerEmail" class="form-control"/>
                </div>
                <div>
                    <span>Numero<label for="customerPhone">*</label></span>
                    <form:input path="cart.customer.customerPhone"  id="customerPhone" class="form-control"/>
                </div>
                <h1>Adresse de facturation</h1>

                <div>
                    <span>Rue<label for="streetname">*</label></span>
                    <form:input path="cart.customer.bilingAddress.streetname"  id="streetname"  class="form-control"/>
                </div>
                <div>
                    <span>Complement adresse<label for="adress">*</label></span>
                    <form:input path="cart.customer.bilingAddress.adress"  id="adress"  class="form-control"/>
                </div>
                <div>
                    <span>Ville<label for="city">*</label></span>
                    <form:input path="cart.customer.bilingAddress.city"  id="city"  class="form-control"/>
                </div>
                <div>
                    <span>Code Postal<label for="zipcode">*</label></span>
                    <form:input path="cart.customer.bilingAddress.zipCode"  id="zipcode"  class="form-control"/>
                </div>
                <div>
                    <span>Departement<label for="state"></label></span>
                    <form:input path="cart.customer.bilingAddress.state"  id="state" class="form-control"/>
                </div>
                <div>
                    <span>Pays<label for="country">*</label></span>
                    <form:input path="cart.customer.bilingAddress.country"  id="country"  class="form-control"/>
                </div>

                <div class="clearfix"></div>
                <input type="hidden" name="_flowExecutionKey"/>

            </div>
            <div class="clearfix"> </div>
            <div class="register-but">
                <input type="submit" value="Suivant" name="_eventId_customerInfoCollected">
                <button class="btn btn-default" name="_eventId_cancel">Annuler</button>
            </div>
        </form:form>

    </div>
</div>
<%@include file="/WEB-INF/views/footer.jsp"%>
