<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 10/04/2016
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 10/04/2016
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>

<div class="content">
    <div class="register">
        <form:form   commandName="order">
            <div class="register-top-grid">
                <h3>Confirmation de commande</h3>

                <div class="well">
                    <address>
                        <h1>Addresse de livraison</h1></address>
                   <p>${order.cart.customer.shippingAddress.streetname}</p> </br>
                    <p>${order.cart.customer.shippingAddress.adress}</p></br>
                    <p>${order.cart.customer.shippingAddress.zipCode}</p></br>
                    <p>${order.cart.customer.shippingAddress.city}</p></br>
                    <p>${order.cart.customer.shippingAddress.state}</p></br>
                    <p>${order.cart.customer.shippingAddress.country}</p>

                    </address>
                    <date>
                        <p>Date d'achat : <fmt:formatDate value="${now}"/></p>
                    </date>

                    <address>
                        <h1>Addresse de facturation</h1></address>
                    <p>${order.cart.customer.bilingAddress.streetname}</p> </br>
                    <p>${order.cart.customer.bilingAddress.adress}</p></br>
                    <p>${order.cart.customer.bilingAddress.zipCode}</p></br>
                    <p>${order.cart.customer.bilingAddress.city}</p></br>
                    <p>${order.cart.customer.bilingAddress.state}</p></br>
                    <p>${order.cart.customer.bilingAddress.country}</p>

                    </address>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTable">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Titre</th>
                                    <th>Quantite</th>
                                    <th>Prix</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${order.cart.cartItems}" var="cartItem">
                                    <tr class="odd gradeX">
                                        <td><img src='<c:url value="/resources/images/movies/${cartItem.movie.picture_name}" />' alt="${cartItem.movie.title}" class="img-responsive" style="width:357px;height:179px" /></td>
                                        <td>${cartItem.movie.title}</td>
                                        <td>${cartItem.quantity}</td>
                                        <td>${cartItem.movie.price}</td>
                                        <td>${cartItem.totalPrice}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <div>
                                <p>Montant Total</p>
                                <p>${order.cart.grandTotal}</p>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <input type="hidden" name="_flowExecutionKey"/>

            </div>
            <div class="clearfix"> </div>
            <div class="register-but">
                <button class="btn btn-default" name="_eventId_backToCollectShippingDetail">Retour</button>
                <input type="submit" value="Commander" name="_eventId_orderConfirmed">
                <button class="btn btn-default" name="_eventId_cancel">Annuler</button>
            </div>
        </form:form>

    </div>
</div>
<%@include file="/WEB-INF/views/footer.jsp"%>






