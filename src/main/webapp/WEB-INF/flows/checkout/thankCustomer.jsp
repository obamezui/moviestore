<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 10/04/2016
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>

<div class="content-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <h1>Merci pour votre commande</h1>
                <p>vous recevrez un mail contenant recapitulatif de votre commande</p>

            </div>

        </section>
        <section>
            <a href='<spring:url value="/" />' class="btn btn-default">ok</a>

        </section>


    </div>

</div>
<%@include file="/WEB-INF/views/footer.jsp"%>