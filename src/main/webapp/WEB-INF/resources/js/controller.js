/**
 * Created by mezui on 31/03/2016.
 * le controller angular qui va nous permettre
 * de communiquer avec l'api rest
 */


var cartApp = angular.module("cartApp",['LocalStorageModule']);
cartApp.config(function(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('moviestore');

            })
/*cartApp.config(function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.xsrfCookieName  = '';
    $httpProvider.defaults.xsrfHeaderName  = 'csrftoken';
});*/
/*cartApp.config(function(csrfProvider) {
    // optional configurations
    csrfProvider.config({
        url: '/rest/cart',
        maxRetries: 2,
        csrfHttpType: 'head',
        csrfTokenHeader: 'X-CSRF-XXX-TOKEN',
        httpTypes: ['PUT', 'POST', 'DELETE'] //CSRF token will be added only to these method types
    });
})
cartApp.run();*/
.controller("cartCtrl",function($scope,$http,localStorageService){
$scope.cart=localStorageService.get('data');
    $scope.cartId=localStorageService.get('cartId');

 $scope.majcart = function() {


     $http.get('/webapp/rest/cart/'+""+$scope.cartId+"").success(function (data) {
         localStorageService.set('data', data);
         $scope.cart= localStorageService.get('data');
         $scope.cart=data;

         });

 }

     $scope.clearCart = function(){

         $http.delete('/webapp/rest/cart/'+$scope.cartId)
             .success($scope.majcart());


     }

    $scope.initCart = function(cartId){
        localStorageService.set('cartId', cartId);
        $scope.majcart();


    }

    $scope.addToCart = function(movieId){
        $http.put('/webapp/rest/cart/add/'+""+movieId+"").success(function(){
            $scope.majcart();
            alert("le film a bien été ajouté au panier");
        });




    }

    $scope.removeFromCart = function(movieId){
        $http.put('/webapp/rest/cart/remove/'+""+movieId+"").success(function(){
            $scope.majcart();
            alert("le film a bien été supprimé du panier");
        });

    }

    $scope.calculGrandTotal = function(){

      $scope.grandTotal=0;

        for(var i=0; i< $scope.cart.cartItems.length;i++){
            $scope.grandTotal += $scope.cart.cartItems[i].totalPrice;

        }

        return $scope.grandTotal;

    }





});
