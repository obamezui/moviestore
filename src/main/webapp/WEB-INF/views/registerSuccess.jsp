<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 05/04/2016
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

                Votre inscription a bien été faites dans notre base de données client
                <a href='<spring:url value="/movie/movielist"/>'>Commencez vos achats</a>
            </div>
        </div>
    </div>
<%@include file="/WEB-INF/views/footer.jsp"%>
