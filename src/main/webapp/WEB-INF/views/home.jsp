<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 29/03/2016
  Time: 12:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>
        <div class="header_top">
            <div class="col-sm-3 logo"><a href="index.html"><img src='<c:url value="/resources/images/logo.png" />' alt=""/></a></div>
            <div class="clearfix"> </div>
        </div>
        <div class="slider">
            <div class="callbacks_container">
            <ul class="rslides" id="slider">
                <c:forEach items="${movies}" var="movie">


                        <li><img src='<c:url value="/resources/images/movies/${movie.picture_name}" />' class="img-responsive" alt="${movie.title}"/>
                            <div class="button">
                                <a href="<spring:url value="/movie/viewmovie/${movie.id}" />" class="hvr-shutter-out-horizontal">voir la fiche</a>
                            </div>
                        </li>


                </c:forEach>
            </ul>
            </div>
        </div>
<%@include file="/WEB-INF/views/footer.jsp"%>
