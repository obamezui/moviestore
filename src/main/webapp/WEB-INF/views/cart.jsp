<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 02/04/2016
  Time: 02:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>
<div class="container" ng-app="cartApp">
    <div class="row">
        <div ng-controller="cartCtrl"  ng-init="initCart('${cartId}')" class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Film</th>
                    <th>Quantité</th>
                    <th class="text-center">Prix</th>
                    <th class="text-center">Total</th>
                    <th> </th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in cart.cartItems">
                    <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src='/webapp/resources/images/movies/{{item.movie.picture_name}}' style="width:357px;height:179px""> </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#">{{item.movie.title}}</a></h4>
                            </div>
                        </div></td>
                    <td class="col-sm-1 col-md-1" style="text-align: center">
                        <input type="email" class="form-control" id="quantity" value="{{item.quantity}}">
                    </td>
                    <td class="col-sm-1 col-md-1 text-center"><strong>{{item.movie.price}}</strong></td>
                    <td class="col-sm-1 col-md-1 text-center"><strong>{{item.movie.total}}</strong></td>
                    <td class="col-sm-1 col-md-1">
                        <a href="#" ng-click="removeFromCart(item.cartItemId)" type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> Supprimer
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>   </td>
                    <td>   </td>
                    <td>   </td>
                    <td><h3>Total</h3></td>
                    <td class="text-right"><h3><strong>{{calculGrandTotal()}}</strong></h3></td>
                </tr>
                <tr>
                    <td>   </td>
                    <td>   </td>
                    <td>   </td>
                    <td>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Continuer votre shopping
                        </button></td>
                    <td>
                    <a href='<spring:url value="/order/${cartId}"/>' class="btn btn-success">
                        Passer commande <span class="glyphicon glyphicon-play"></span>
                    </a>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src='<c:url value="/resources/js/controller.js"/>'></script>
<%@include file="/WEB-INF/views/footer.jsp"%>
