<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 05/04/2016
  Time: 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>

<div class="content">
    <div class="register">
        <form:form action="${pageContext.request.contextPath}/register?${_csrf.parameterName}=${_csrf.token}" method="post"  commandName="customer">
            <div class="register-top-grid">
                <h3>Inscription</h3>
                <div>
                    <span>Nom<label for="customerName">*</label></span>
                     <form:input path="customerName"  id="customerName" class="form-control"/>
                </div>
                <div>
                <span>Email<label for="customerEmail">*</label></span>
                <form:input path="customerEmail"  id="customerEmail" class="form-control"/>
                </div>
                <div>
                <span>Numero<label for="customerPhone">*</label></span>
                <form:input path="customerPhone"  id="customerPhone" class="form-control"/>
                </div>
                <div>
                <span>Login<label for="username">*</label></span>
                <form:input path="username"  id="username" class="form-control"/>
                </div>
                <div>
                <span>Password<label for="password">*</label></span>
                <form:input path="password"  id="password" type="password" class="form-control"/>
                </div>
                <div class="clearfix"></div>
                <h1>Adresse de facturation</h1>

                <div>
                <span>Rue<label for="streetname">*</label></span>
                <form:input path="bilingAddress.streetname"  id="streetname"  class="form-control"/>
                </div>
                <div>
                <span>Complement adresse<label for="adress">*</label></span>
                <form:input path="bilingAddress.adress"  id="adress"  class="form-control"/>
                </div>
                <div>
                <span>Ville<label for="city">*</label></span>
                <form:input path="bilingAddress.city"  id="city"  class="form-control"/>
                </div>
                <div>
                <span>Code Postal<label for="zipcode">*</label></span>
                <form:input path="bilingAddress.zipCode"  id="zipcode"  class="form-control"/>
                </div>
                <div>
                <span>Departement<label for="state"></label></span>
                <form:input path="bilingAddress.state"  id="state" class="form-control"/>
                </div>
                <div>
                <span>Pays<label for="country">*</label></span>
                <form:input path="bilingAddress.country"  id="country"  class="form-control"/>
                </div>

                <div class="clearfix"></div>
                <h1>Adresse de livraison</h1>
                <div>
                <span>Rue<label for="streetname">*</label></span>
                <form:input path="shippingAddress.streetname"  id="streetname"  class="form-control"/>
                </div>
                <div>
                <span>Complement adresse<label for="adress">*</label></span>
                <form:input path="shippingAddress.adress"  id="adress"  class="form-control"/>
                </div>
                <div>
                <span>Ville<label for="city">*</label></span>
                <form:input path="shippingAddress.city"  id="city"  class="form-control"/>
                </div>
                <div>
                <span>Code Postal<label for="zipcode">*</label></span>
                <form:input path="shippingAddress.zipCode"  id="zipcode"  class="form-control"/>
                </div>
                <div>
                <span>Departement<label for="state"></label></span>
                <form:input path="shippingAddress.state"  id="state" class="form-control"/>
                </div>
                <div>
                <span>Pays<label for="country">*</label></span>
                <form:input path="shippingAddress.country"  id="country"  class="form-control"/>
                </div>

            </div>
            <div class="clearfix"> </div>
            <div class="register-but">
                <input type="submit" value="valider">
            </div>
        </form:form>

    </div>
</div>
<%@include file="/WEB-INF/views/footer.jsp"%>
