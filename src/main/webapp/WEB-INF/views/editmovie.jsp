<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 31/03/2016
  Time: 16:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/adminheader.jsp"%>
<form:form action="${pageContext.request.contextPath}/admin/movie/edit?${_csrf.parameterName}=${_csrf.token}" method="post" enctype="multipart/form-data" commandName="movie">
    <div class="form-group">
        <label for="title">Titre</label>
        <form:input path="title"  id="title" value="${movie.title}" class="form-control"/>
        <form:hidden path="id"   value="${movie.id}" />
        <form:hidden path="picture_name"   value="${movie.picture_name}" />
    </div>
    <div class="form-group">
        <label for="picture">Image</label>
        <form:input path="picture"  id="picture" type="file" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="price">Prix</label>
        <form:input path="price"  id="price" value="${movie.price}" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <form:textarea path="description"  id="description" value="${movie.description}" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="release_year">Date realisation</label>
        <form:input path="release_year"  id="release_year" value="${movie.release_year}" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="time">Duree</label>
        <form:input path="time"  id="time" value="${movie.time}" class="form-control"/>

    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Editer</button>

    </div>
</form:form>
<%@include file="/WEB-INF/views/adminfooter.jsp"%>
