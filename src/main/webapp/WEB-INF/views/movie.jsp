<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 31/03/2016
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>
<div  ng-app="cartApp" class="content">
    <div  class="movie_top">
        <div ng-controller="cartCtrl" class="col-md-12 movie_box">
            <div class="grid images_3_of_2">
                <div class="movie_image">
                    <span class="movie_rating">5.0</span>
                    <img src='<c:url value="/resources/images/movies/${movie.picture_name}" />' class="img-responsive" alt="${movie.picture_name}" />
                </div>
               <!-- <div class="movie_rate">
                    <div class="rating_desc"><p>Your Vote :</p></div>
                    <form action="" class="sky-form">
                        <fieldset>
                            <section>
                                <div class="rating">
                                    <input type="radio" name="stars-rating" id="stars-rating-5">
                                    <label for="stars-rating-5"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-4">
                                    <label for="stars-rating-4"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-3">
                                    <label for="stars-rating-3"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-2">
                                    <label for="stars-rating-2"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-1">
                                    <label for="stars-rating-1"><i class="icon-star"></i></label>
                                </div>
                            </section>
                        </fieldset>
                    </form>
                    <div class="clearfix"> </div>
                </div>-->
            </div>
            <div class="desc1 span_3_of_2">
                <p class="movie_option"><strong>Pays: </strong>
                <p class="movie_option"><strong>Annee: </strong>2014</p>
                <p class="movie_option"><strong>Categorie: </strong>
                <p class="movie_option"><strong>Date de production: </strong>${movie.release_year}</p>
                <p class="movie_option"><strong>Director: </strong><a href="#">fernand </a></p>
                <p class="movie_option"><strong>Actors: </strong><a href="#">anything</a>, <a href="#">Lorem Ipsum</a>, <a href="#" discovered<="" a="">, </a><a href="#"> Virginia</a>, <a href="#"> Virginia</a>, <a href="#">variations</a>, <a href="#">variations</a>, <a href="#">variations</a>, <a href="#"> Virginia</a> <a href="#">...</a></p>
                <div class="down_btn"><a class="btn1" href="#"><span> </span>ajouter au panier</a></div>
            </div>
            <div class="clearfix"></div>
            <c:set var="role" scope="page" value="${param.role}"/>
            <c:set var="url" scope="page" value="/" />
            <c:if test="${role='admin'}">
                <c:set var="url" scope="page" value="/admin" />
            </c:if>
            <p >
                <a href='<c:url value="${url}" />' class="btn btn-default">Retour</a>
                <a href='#' class="btn btn-default" ng-click="addToCart('${movie.id}')">
                    <span class="glyphicon glyphicon-shopping-cart"></span>Commander
                </a>
                <a href='<spring:url value="/cart" />' class="glyphicon glyphicon-hand-right">panier</a>
            </p>
        </div>
    </div>
</div>
<script src='<c:url value="/resources/js/controller.js"/>'></script>
<%@include file="/WEB-INF/views/footer.jsp"%>