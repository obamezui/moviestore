<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 30/03/2016
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Movie_store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='<c:url value="/resources/css/bootstrap.css"  />' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/css/style.css"  />' rel="stylesheet" type="text/css" media="all" />
    <!-- start plugins -->
    <script type="text/javascript" src='<c:url value="/resources/js/jquery-1.11.1.min.js"  />'></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <script src='<c:url value="/resources/js/responsiveslides.min.js"  />'></script>
</head>
<body>
<div class="container">
    <div class="container_wrap">
        <div class="content">
            <div class="register">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <h3>Login</h3>
                    <c:if test="${not empty msg}" >
                        <p>${msg}</p>
                    </c:if>
                    <c:if test="${not empty error}" >
                        <p>${error}</p>
                    </c:if>
                    <form name="loginForm" action='<c:url value="/login" />' method="post">
                        <div>
                            <span>User :<label for="username">*</label></span>
                            <input type="text" id="username" name="username">
                        </div>
                        <div>
                            <span>Password :<label for="password">*</label></span>
                            <input type="password" id="password" name="password">
                        </div>
                        <a class="forgot" href="#">Forgot Your Password?</a>
                        <input type="submit" value="Login">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    </form>
                </div>
                <div class="col-md-2">
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
