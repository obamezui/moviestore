<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 31/03/2016
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/views/header.jsp"%>
    <div class="content">
        <h2 class="m_3">les meilleurs films  </h2>
        <div class="movie_top">
            <div class="col-md-12 movie_box">
                <c:forEach items="${movies}" var="movie">
                    <div class="movie movie-test movie-test-dark movie-test-left">
                        <div class="movie__images">
                            <a href='<spring:url value="/movie/viewmovie/${movie.id}"/>' class="movie-beta__link">
                                <img alt="" src='<c:url value="/resources/images/movies/${movie.picture_name}" />' class="img-responsive" alt="${movie.title}"/>
                            </a>
                        </div>
                        <div class="movie__info">
                            <a href="single.html" class="movie__title">${movie.title} (${movie.release_year})  </a>
                            <p class="movie__time">${movie.title}</p>
                            <!--  <p class="movie__option"><a href="single.html">Contray</a> | <a href="single.html">Dolor sit</a> | <a href="single.html">Drama</a></p>-->
                            <!-- <ul class="list_6">
                                 <li><i class="icon1"> </i><p>2,548</p></li>
                                 <li><i class="icon3"> </i><p>546</p></li>
                                 <li>Rating : &nbsp;&nbsp;<p><img src="images/rating1.png" alt=""></p></li>
                                 <div class="clearfix"> </div>
                             </ul>-->
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </c:forEach>

            </div>
        </div>
    </div>
<%@include file="/WEB-INF/views/footer.jsp"%>
