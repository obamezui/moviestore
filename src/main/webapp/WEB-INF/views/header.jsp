<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mezui
  Date: 31/03/2016
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Movie Store : site d'achat de videos en ligne</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Movie_store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='<c:url value="/resources/css/bootstrap.css"  />' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/css/style.css"  />' rel="stylesheet" type="text/css" media="all" />
    <!-- start plugins -->
    <script type="text/javascript" src='<c:url value="/resources/js/jquery-1.11.1.min.js"  />'></script>
    <script type="text/javascript" src='<c:url value="/resources/js/angular.min.js"  />'></script>
    <script type="text/javascript" src='<c:url value="/resources/js/angular-local-storage.js"  />'></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <script src='<c:url value="/resources/js/responsiveslides.min.js"  />'></script>
    <script>
        $(function () {
            $("#slider").responsiveSlides({
                auto: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>
</head>
<body>
<div class="container">
    <div class="container_wrap">
        <c:if test="${pageContext.request.userPrincipal.name !=null}" >
            <div class="col-sm-3 header_right">
                <ul class="header_right_box">
                    <li><img src="images/p1.png" alt=""/></li>
                    <li><p><a href='<c:url value="/login"/>'>Salut ${pageContext.request.userPrincipal.name}</a></p></li>
                    <li>
                        <a href='<c:url value="/login"/>'>logout
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </li>
                    <div class="clearfix"> </div>
                </ul>
            </div>


        </c:if>

        <div class="clearfix"> </div>
