package com.moviestore.Service;

import com.moviestore.model.Cart;
import org.springframework.stereotype.Service;

/**
 * Created by mezui on 07/04/2016.
 */
@Service
public interface CartService {
    Cart getCartById(int cartId);
    void update(Cart cart);
}
