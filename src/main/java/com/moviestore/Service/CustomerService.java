package com.moviestore.Service;

import com.moviestore.model.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mezui on 05/04/2016.
 */
@Service
public interface CustomerService {
    void addCustomer(Customer customer);
    Customer getCustomer(int customerId);
    List<Customer> getAllcustomer();
    Customer getCustomerByUsername(String username);
}
