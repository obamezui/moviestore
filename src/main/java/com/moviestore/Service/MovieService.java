package com.moviestore.Service;

import com.moviestore.model.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mezui on 04/04/2016.
 */
@Service
public interface MovieService {
    List<Movie> getMovies();
    Movie getMovie(String id);
    void addMovie(Movie movie);
    void editMovie(Movie movie);
    void deleteMovie(Movie movie);


}
