package com.moviestore.Service;

import com.moviestore.model.Cart;
import com.moviestore.model.CartItem;
import org.springframework.stereotype.Service;

/**
 * Created by mezui on 07/04/2016.
 */
@Service
public interface CartItemService {
    void addCartItem(CartItem cartItem);
    void removeCartItem(CartItem cartItem);
    void removeAll(Cart cart);
    CartItem getCartItemById(int cartItemId);

}
