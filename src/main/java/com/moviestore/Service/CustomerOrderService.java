package com.moviestore.Service;

import com.moviestore.model.CustomerOrder;
import org.springframework.stereotype.Service;

/**
 * Created by mezui on 08/04/2016.
 */
@Service
public interface CustomerOrderService {
    void addCustomerOrder(CustomerOrder customerOrder);
    double getCustomerOrderGrandTotal(int cartId);

}
