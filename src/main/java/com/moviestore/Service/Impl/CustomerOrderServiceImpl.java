package com.moviestore.Service.Impl;

import com.moviestore.Service.CartService;
import com.moviestore.Service.CustomerOrderService;
import com.moviestore.dao.CustomerOrderDao;
import com.moviestore.model.Cart;
import com.moviestore.model.CartItem;
import com.moviestore.model.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mezui on 08/04/2016.
 */
@Service
public class CustomerOrderServiceImpl implements CustomerOrderService{
    @Autowired
    private CustomerOrderDao customerOrderDao;
    @Autowired
    private CartService cartService;
    public void addCustomerOrder(CustomerOrder customerOrder) {
        customerOrderDao.addCustomerOrder(customerOrder);

    }

    public double getCustomerOrderGrandTotal(int cartId) {
        double grandTotal=0;
        Cart cart = cartService.getCartById(cartId);
        List<CartItem> cartItems = cart.getCartItems();
        for (CartItem item: cartItems){
            grandTotal+= item.getTotalPrice();
        }
        return grandTotal;
    }
}
