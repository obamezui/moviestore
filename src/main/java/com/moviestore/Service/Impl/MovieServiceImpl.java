package com.moviestore.Service.Impl;

import com.moviestore.Service.MovieService;
import com.moviestore.dao.MovieDao;
import com.moviestore.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mezui on 04/04/2016.
 */
@Service
public class MovieServiceImpl  implements MovieService {
    @Autowired
  private  MovieDao movieDao ;

    public List<Movie> getMovies() {
        return movieDao.getMovies();
    }

    public Movie getMovie(String id) {

        return movieDao.getMovie(id);
    }

    public void addMovie(Movie movie) {
        movieDao.addMovie(movie);

    }

    public void editMovie(Movie movie) {
        movieDao.editMovie(movie);
    }

    public void deleteMovie(Movie movie) {
       // movieDao.deleteMovie(movie.getId());
    }
}
