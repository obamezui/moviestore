package com.moviestore.Service.Impl;

import com.moviestore.Service.CartService;
import com.moviestore.dao.CartDao;
import com.moviestore.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mezui on 07/04/2016.
 */
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private CartDao cartDao;
    public Cart getCartById(int cartId) {
        return cartDao.getCartById(cartId);
    }

    public void update(Cart cart) {
        cartDao.update(cart);

    }
}
