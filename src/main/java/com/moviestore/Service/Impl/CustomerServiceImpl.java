package com.moviestore.Service.Impl;

import com.moviestore.Service.CustomerService;
import com.moviestore.dao.CustomerDao;
import com.moviestore.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mezui on 05/04/2016.
 */
@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerDao customerDao;
    public void addCustomer(Customer customer) {
        customerDao.addCustomer(customer);
    }

    public Customer getCustomer(int customerId) {

        return customerDao.getCustomer(customerId);
    }

    public List<Customer> getAllcustomer() {
        return customerDao.getAllcustomer();
    }

    public Customer getCustomerByUsername(String username) {
        return customerDao.getCustomerByUsername(username);
    }
}
