package com.moviestore.Service.Impl;

import com.moviestore.Service.CartItemService;
import com.moviestore.dao.CartItemDao;
import com.moviestore.model.Cart;
import com.moviestore.model.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mezui on 07/04/2016.
 */
@Service
public class CartItemServiceImpl implements CartItemService {
    @Autowired
    private CartItemDao cartItemDao;
    public void addCartItem(CartItem cartItem) {
        cartItemDao.addCartItem(cartItem);
    }

    public void removeCartItem(CartItem cartItem) {
        cartItemDao.removeCartItem(cartItem);
    }

    public void removeAll(Cart cart) {
        cartItemDao.removeAll(cart);

    }

    public CartItem getCartItemById(int cartItemId) {
        return cartItemDao.getCartItemById(cartItemId);
    }
}
