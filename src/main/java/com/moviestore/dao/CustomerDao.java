package com.moviestore.dao;

import com.moviestore.model.Customer;

import java.util.List;

/**
 * Created by mezui on 05/04/2016.
 */
public interface CustomerDao {
    void addCustomer(Customer customer);
    Customer getCustomer(int customerId);
    List<Customer> getAllcustomer();
    Customer getCustomerByUsername(String username);
}
