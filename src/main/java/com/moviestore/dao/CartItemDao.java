package com.moviestore.dao;

import com.moviestore.model.Cart;
import com.moviestore.model.CartItem;

/**
 * Created by mezui on 07/04/2016.
 */
public interface CartItemDao {
    void addCartItem(CartItem cartItem);
    void removeCartItem(CartItem cartItem);
    void removeAll(Cart cart);
    CartItem getCartItemById(int cartItemId);
}
