package com.moviestore.dao;

import com.moviestore.model.Cart;

import java.io.IOException;

/**
 * Created by mezui on 31/03/2016.
 */
public interface CartDao {
    Cart getCartById(int cartId);
    Cart validate(int cartId) throws IOException;
    void update(Cart cart);
}
