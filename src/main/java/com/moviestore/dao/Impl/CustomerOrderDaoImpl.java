package com.moviestore.dao.Impl;

import com.moviestore.Service.CustomerOrderService;
import com.moviestore.dao.CustomerOrderDao;
import com.moviestore.model.CustomerOrder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mezui on 08/04/2016.
 */
@Repository
@Transactional
public class CustomerOrderDaoImpl implements CustomerOrderDao{
    @Autowired
    private SessionFactory sessionFactory;
    public void addCustomerOrder(CustomerOrder customerOrder) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(customerOrder);
        session.flush();

    }

    public double getCustomerOrderGrandTotal(int cartId) {
        return 0;
    }
}
