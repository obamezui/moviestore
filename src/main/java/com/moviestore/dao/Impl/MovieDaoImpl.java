package com.moviestore.dao.Impl;

import com.moviestore.dao.MovieDao;
import com.moviestore.model.Movie;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mezui on 30/03/2016.
 */
@Repository
@Transactional
public class MovieDaoImpl  implements MovieDao {

    @Autowired
   private  SessionFactory sessionFactory;


    public List<Movie> getMovies() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from  Movie");
        List<Movie> movieList = query.list();
        session.flush();

        return movieList;
    }

    public Movie getMovie(String id) {
        Session session = sessionFactory.getCurrentSession();
        Movie movie = (Movie)session.get(Movie.class,id);
        session.flush();
        return movie;
    }

    public void addMovie(Movie movie) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(movie);
        session.flush();

    }

    public void editMovie(Movie movie) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(movie);
        session.flush();

    }

    public void deleteMovie(Movie movie) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(movie);
        session.flush();


    }
}
