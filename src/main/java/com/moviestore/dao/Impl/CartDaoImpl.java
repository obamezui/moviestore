package com.moviestore.dao.Impl;

import com.moviestore.Service.CustomerOrderService;
import com.moviestore.dao.CartDao;
import com.moviestore.model.Cart;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mezui on 31/03/2016.
 */
@Repository
@Transactional
public class CartDaoImpl implements CartDao{
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    CustomerOrderService customerOrderService;

    public Cart getCartById(int cartId) {
        Session session = sessionFactory.getCurrentSession();
        return  session.get(Cart.class,cartId);
    }

    public Cart validate(int cartId) throws IOException {
        Cart cart = getCartById(cartId);
        if(cart ==null || cart.getCartItems().size()==0){
            throw new IOException(cartId+"");
        }
        update(cart);
        return  cart;
    }

    public void update(Cart cart) {
        int cartId = cart.getCartId();
        Session session = sessionFactory.getCurrentSession();
        double grandTotal = customerOrderService.getCustomerOrderGrandTotal(cartId);
        cart.setGrandTotal(grandTotal);
        session.saveOrUpdate(cart);
        session.flush();

    }
}
