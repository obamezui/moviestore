package com.moviestore.dao.Impl;

import com.moviestore.dao.CartItemDao;
import com.moviestore.model.Cart;
import com.moviestore.model.CartItem;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mezui on 07/04/2016.
 */
@Repository
@Transactional
public class CartItemDaoImpl  implements CartItemDao{
    @Autowired
    private SessionFactory sessionFactory;

    public void addCartItem(CartItem cartItem) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(cartItem);
        session.flush();


    }

    public void removeCartItem(CartItem cartItem) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(cartItem);
        session.flush();

    }

    public void removeAll(Cart cart) {
        List<CartItem> cartItems = cart.getCartItems();
        for(CartItem item: cartItems){
            removeCartItem(item);

        }

    }

    public CartItem getCartItemById(int cartItemId) {
        Session session = sessionFactory.getCurrentSession();


        return  session.get(CartItem.class,cartItemId);
    }
}
