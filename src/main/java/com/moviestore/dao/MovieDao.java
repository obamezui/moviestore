package com.moviestore.dao;

import com.moviestore.model.Movie;

import java.util.List;

/**
 * Created by mezui on 30/03/2016.
 */
public interface MovieDao {
    List<Movie> getMovies();
    Movie getMovie(String id);
    void addMovie(Movie movie);
    void editMovie(Movie movie);
    void deleteMovie(Movie movie);

}
