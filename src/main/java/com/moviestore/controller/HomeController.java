package com.moviestore.controller;

import com.moviestore.Service.MovieService;
import com.moviestore.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by mezui on 04/04/2016.
 */
@Controller
public class HomeController {
    @Autowired
    MovieService movieService;

    @RequestMapping("/")
    public String home(Model model){
        List<Movie> movies = movieService.getMovies();
        model.addAttribute("movies",movies);

        return "home";
    }

    @RequestMapping("/login")
    public String login(@RequestParam(value="error",required=false) String error ,@RequestParam(value="logout",required=false) String logout,
                        Model model ) {
        if(error!=null){
            model.addAttribute("error","identifiants invalides ");
        }

        if(logout!=null){
            model.addAttribute("msg","connection reussie");
        }

        return "login";
    }

}
