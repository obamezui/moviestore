package com.moviestore.controller;

import com.moviestore.Service.CartItemService;
import com.moviestore.Service.CartService;
import com.moviestore.Service.CustomerService;
import com.moviestore.Service.MovieService;
import com.moviestore.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by mezui on 07/04/2016.
 */
@RestController
@RequestMapping("/rest/cart")
public class CartResources {
    @Autowired
    private CartService cartService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private CartItemService cartItemService;
    @RequestMapping("/{cartId}")
   public Cart getCartById(@PathVariable(value="cartId") int cartId){
        return cartService.getCartById(cartId);
    }

    @RequestMapping(value="/add/{movieId}",method = RequestMethod.PUT)
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_USER')")

    public void addItem(@PathVariable(value="movieId") String movieId , @AuthenticationPrincipal UserDetails activeUser){
        Customer customer = customerService.getCustomerByUsername( activeUser.getUsername());

        Cart cart = customer.getCart();
        Movie movie = movieService.getMovie(movieId);
        List<CartItem> cartItems = cart.getCartItems();
        for(int i=0;i<cartItems.size();i++){
            if(movie.getId().equals(cartItems.get(i).getMovie().getId()) ){
                CartItem cartItem = cartItems.get(i);
                cartItem.setQuantity(cartItem.getQuantity()+1);
                cartItem.setTotalPrice(movie.getPrice().multiply(new BigDecimal(cartItem.getQuantity())).doubleValue());
                cartItemService.addCartItem(cartItem);
                return;

            }

        }

        CartItem cartItem = new CartItem();
        cartItem.setMovie(movie);
        cartItem.setQuantity(1);
        cartItem.setTotalPrice(movie.getPrice().multiply(new BigDecimal(cartItem.getQuantity())).doubleValue());
        cartItem.setCart(cart);
        cartItemService.addCartItem(cartItem);


    }

    @RequestMapping(value="/remove/{movieId}",method = RequestMethod.PUT)
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    public void removeItem(@PathVariable(value="movieId") int cartItemId ){
        CartItem cartItem = cartItemService.getCartItemById(cartItemId);
        cartItemService.removeCartItem(cartItem);

    }

    @RequestMapping(value="/{cartId}",method = RequestMethod.DELETE)
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    public void clearCartItem(@PathVariable(value="cartId") int cartId){
        Cart cart = cartService.getCartById(cartId);
        cartItemService.removeAll(cart);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="erreur de request verifier vos données ")
    public void handleClientErrors(Exception e){
        e.printStackTrace();
    }

   @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,reason="erreur de serveur ")
    public void handleServerErrors(Exception e){
        e.printStackTrace();
    }


}
