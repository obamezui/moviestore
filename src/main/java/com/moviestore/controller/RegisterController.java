package com.moviestore.controller;

import com.moviestore.Service.CustomerService;
import com.moviestore.model.BilingAddress;
import com.moviestore.model.Customer;
import com.moviestore.model.ShippingAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mezui on 05/04/2016.
 */
@Controller
public class RegisterController {
    @Autowired
   private CustomerService customerService;
    @RequestMapping("/register")
    public String registerCustomer(Model model){
        Customer customer = new Customer();
        BilingAddress bilingAddress = new BilingAddress();
        ShippingAddress shippingAddress = new ShippingAddress();
        customer.setBilingAddress(bilingAddress);
        customer.setShippingAddress(shippingAddress);
        model.addAttribute("customer",customer);
        return "register";
    }

    @RequestMapping(value="/register",method= RequestMethod.POST)
    public String registerCustomerPost(@ModelAttribute("customer") Customer customer, Model model){
        customer.setEnabled(true);
        customerService.addCustomer(customer);
        return "registerSuccess";

    }
}
