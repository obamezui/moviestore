package com.moviestore.controller.admin;

import com.moviestore.Service.MovieService;
import com.moviestore.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by mezui on 04/04/2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminHome {
    private Path path;
    @Autowired
    private MovieService movieService;
    @RequestMapping
    public String adminPage(){
        return "admin";
    }

    @RequestMapping("/movielist")
    public String movieInventory(Model model){
        List<Movie> movies = movieService.getMovies();
        model.addAttribute("movies",movies);
        return "inventory";
    }

    @RequestMapping("/movie/addmovie")
    public String addMovie(Model model){
        Movie movie = new Movie();
        model.addAttribute("movie",movie);
        return "addmovie";
    }

    @RequestMapping("/movie/editmovie/{id}")
    public String editMovie(@PathVariable() String id, Model model){
        Movie movie = movieService.getMovie(id);
        model.addAttribute("movie",movie);
        return "editmovie";
    }


    @RequestMapping(value="/movie/addmovie",method = RequestMethod.POST)
    public String addMoviePost(@Valid @ModelAttribute ("movie")Movie movie, BindingResult result , HttpServletRequest request ){
        if(result.hasErrors()){
            return "addmovie";
        }



        MultipartFile movieImage = movie.getPicture();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory+"\\WEB-INF\\resources\\images\\movies\\"+movieImage.getOriginalFilename());

        if(movieImage!=null && !movieImage.isEmpty()){
            try{
                movieImage.transferTo(new File(path.toString()));
                movie.setPicture_name(movieImage.getOriginalFilename());
            }
            catch (Exception e){
                e.printStackTrace();
                throw new RuntimeException("Echec du transfert de l'image ",e);
            }
        }
        movieService.addMovie(movie);
        return "redirect:/admin/movielist";
    }


    @RequestMapping("/movie/delete/{id}")
    public String deleteMovie (@PathVariable String id, BindingResult result , HttpServletRequest request ) {
        Movie movie = movieService.getMovie(id);
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory+"\\WEB-INF\\resources\\images\\movies\\"+movie.getPicture_name());
        if (Files.exists(path)){
            try{
                Files.delete(path);

            }catch(IOException e){
                e.printStackTrace();
            }
        }

        movieService.deleteMovie(movie);
        return "redirect:/admin/movielist";

    }


    @RequestMapping(value="/movie/editmovie",method = RequestMethod.POST)
    public String editMoviePost(@Valid @ModelAttribute ("movie")Movie movie, BindingResult result , HttpServletRequest request ){
        if(result.hasErrors()){
            return "editmovie";
        }



        MultipartFile movieImage = movie.getPicture();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory+"\\WEB-INF\\resources\\images\\movies\\"+movieImage.getOriginalFilename());

        if(movieImage!=null && !movieImage.isEmpty()){
            try{
                movieImage.transferTo(new File(path.toString()));
                movie.setPicture_name(movieImage.getOriginalFilename());
            }
            catch (Exception e){
                e.printStackTrace();
                throw new RuntimeException("Echec du transfert de l'image ",e);
            }
        }
        movieService.editMovie(movie);
        return "redirect:/admin/movielist";
    }


}
