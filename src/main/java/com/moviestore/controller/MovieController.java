package com.moviestore.controller;

import com.moviestore.Service.MovieService;
import com.moviestore.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by mezui on 04/04/2016.
 */
@Controller
@RequestMapping("/movie")
public class MovieController {
    @Autowired
   private MovieService movieService;
    @RequestMapping("/movielist")
    public String getMovies(Model model){
        List<Movie> movies = movieService.getMovies();
        model.addAttribute("movies",movies);
        return "movielist";

    }

    @RequestMapping("/viewmovie/{id}")
    public String viewMovie(@PathVariable String id, Model model){
        Movie movie = movieService.getMovie(id);
        model.addAttribute("movie",movie);
        return "viewMovie";


    }

}
