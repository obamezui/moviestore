package com.moviestore.controller;

import com.moviestore.Service.CartService;
import com.moviestore.Service.CustomerOrderService;
import com.moviestore.model.Cart;
import com.moviestore.model.Customer;
import com.moviestore.model.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mezui on 08/04/2016.
 */
@Controller
public class OrderController {
    @Autowired
    private CartService cartService;
    @Autowired
    private CustomerOrderService customerOrderService;
    @RequestMapping("/order/{cartId}")
    public String creataOrder(@PathVariable("cartId") int cartId){
        CustomerOrder customerOrder = new CustomerOrder();
        Cart cart = cartService.getCartById(cartId);
        customerOrder.setCart(cart);
        Customer customer = cart.getCustomer();
        customerOrder.setCustomer(customer);
        customerOrder.setBilingAddress(customer.getBilingAddress());
        customerOrder.setShippingAddress(customer.getShippingAddress());
        customerOrderService.addCustomerOrder(customerOrder);
        return "redirect:/checkout?cartId="+cartId;


    }

}
