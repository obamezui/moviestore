package com.moviestore.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.Serializable;

/**
 * Created by mezui on 04/04/2016.
 */
@Entity
public class ShippingAddress implements Serializable{
        private static final long serialVersionUID = 5680816692083329496L;
        @Id
        @GeneratedValue
        private int shippingAddressId;
        private String streetname;
        private String adress;
        private String city;
        private String state;
        private String country;
        private String zipCode;
        @OneToOne
        private Customer customer;


        public int getShippingAddressId() {
            return shippingAddressId;
        }

        public void setShippingAddressId(int shippingAddressId) {
            this.shippingAddressId = shippingAddressId;
        }

        public String getStreetname() {
            return streetname;
        }

        public void setStreetname(String streetname) {
            this.streetname = streetname;
        }

        public String getAdress() {
            return adress;
        }

        public void setAdress(String adress) {
            this.adress = adress;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        @Override
        public String toString() {
            return "ShippingAddress{" +
                    "streetname='" + streetname + '\'' +
                    ", adress='" + adress + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", country='" + country + '\'' +
                    ", zipCode='" + zipCode + '\'' +
                    '}';
        }
}
