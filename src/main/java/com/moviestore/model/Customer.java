package com.moviestore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by mezui on 29/03/2016.
 */
@Entity
public class Customer implements Serializable{

    private static final long serialVersionUID = -5376590306557390656L;
    @Id
    @GeneratedValue
    private int customerId;
    @NotEmpty(message="veuillez renseigner votre nom")
    private String customerName;
    @NotEmpty(message="veuillez renseigner votre email")
    private String customerEmail;
    private String customerPhone;
    @NotEmpty(message="veuillez renseigner votre pseudo")
    private String username;
    @NotEmpty(message="veuillez renseigner le mot passe")
    private String password;
    private boolean enabled;
    @OneToOne
    @JoinColumn(name="BilingAddressId")
    private  BilingAddress bilingAddress;

    @OneToOne
    @JoinColumn(name="ShippingAddressId")
    private  ShippingAddress shippingAddress;

    @OneToOne
    @JoinColumn(name="cartId")
    @JsonIgnore
    private Cart cart;

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public BilingAddress getBilingAddress() {
        return bilingAddress;
    }

    public void setBilingAddress(BilingAddress bilingAddress) {
        this.bilingAddress = bilingAddress;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
