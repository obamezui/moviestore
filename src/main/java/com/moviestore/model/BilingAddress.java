package com.moviestore.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.Serializable;

/**
 * Created by mezui on 04/04/2016.
 */
@Entity
public class BilingAddress implements Serializable{

    private static final long serialVersionUID = 5483936694259503859L;
    @Id
    @GeneratedValue
    private int bilingAddressId;
    private String streetname;
    private String adress;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    @OneToOne
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public int getBilingAddressId() {
        return bilingAddressId;
    }

    public void setBilingAddressId(int bilingAddressId) {
        this.bilingAddressId = bilingAddressId;
    }

    @Override
    public String toString() {
        return "BilingAdress{" +
                "zipCode='" + zipCode + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", adress='" + adress + '\'' +
                ", streetname='" + streetname + '\'' +
                '}';
    }
}
