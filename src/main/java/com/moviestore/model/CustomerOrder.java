package com.moviestore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by mezui on 04/04/2016.
 */
@Entity
public class CustomerOrder implements Serializable{

    private static final long serialVersionUID = 3870706748159994722L;
    @Id
    @GeneratedValue
    private int customerOrderId;

    @OneToOne
    @JoinColumn(name="cartId")
    private Cart cart;

    @OneToOne
    @JoinColumn(name="customerId")
    private Customer customer;

    @OneToOne
    @JoinColumn(name="BilingAddressId")
    private BilingAddress bilingAddress;

    @OneToOne
    @JoinColumn(name="ShippingAddressId")
    private  ShippingAddress shippingAddress;

    public int getCustomerOrderId() {
        return customerOrderId;
    }

    public void setCustomerOrderId(int customerOrderId) {
        this.customerOrderId = customerOrderId;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BilingAddress getBilingAddress() {
        return bilingAddress;
    }

    public void setBilingAddress(BilingAddress bilingAddress) {
        this.bilingAddress = bilingAddress;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
